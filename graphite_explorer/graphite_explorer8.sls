#
search_and_depends:
  pkg.installed:
    - pkgs:
      - collectd-core
      - elasticsearch
      - liblucene4-java
      - python-pysqlite2
      - graphite-carbon
      - graphite-web
      - python-virtualenv

collectd_write_graphite:
  file.managed:
  - name: /etc/collectd/collectd.conf.d/collectd54graphite.conf
  - contents: |
      ##############################################################################
      # Plugin configuration                                                       #
      #----------------------------------------------------------------------------#
      # In this section configuration stubs for each plugin are provided. A desc-  #
      # ription of those options is available in the collectd.conf(5) manual page. #
      ##############################################################################
      
      <Plugin write_graphite>
      	<Node "cen1">
      		Host "localhost"
      		Port "2003"
      		Protocol "tcp"
      		LogSendErrors true
      		Prefix "cd"
      		Postfix ""
      		StoreRates true
      		AlwaysAppendDS false
      		EscapeCharacter "_"
      	</Node>
      </Plugin>
      
  - backup: minion

collectd_network_central:
  file.managed:
  - name: /etc/collectd/collectd.conf.d/network54on25826.conf
  - contents: |
      <Plugin network>
      #	# client setup:
      #	Server "ff18::efc0:4a42" "25826"
      #	<Server "239.192.74.66" "25826">
      #		SecurityLevel Encrypt
      #		Username "user"
      #		Password "secret"
      #		Interface "eth0"
      #	</Server>
      #	TimeToLive "128"
      #
      	# server setup:
      	Listen "*" "25826"
      	<Listen "*" "25826">
      		SecurityLevel Sign
      		AuthFile "/etc/collectd/deb8auth"
      		#SecurityLevel None
      		#Interface "eth0"
      	</Listen>
      #	Listen "ff18::efc0:4a42" "25826"
      #	<Listen "239.192.74.66" "25826">
      #		SecurityLevel Sign
      #		AuthFile "/etc/collectd/passwd"
      #		Interface "eth0"
      #	</Listen>
      	MaxPacketSize 1024
      
      	# proxy setup (client and server as above):
      	Forward true
      
      	# statistics about the network plugin itself
      	ReportStats true
      
      	# "garbage collection"
      #	CacheFlush 1800
      </Plugin>
      
  - backup: minion

collectd_processes:
  file.managed:
  - name: /etc/collectd/collectd.conf.d/processes.conf
  - contents: |
      <Plugin processes>
        Process "cron"
        #Process "/usr/sbin/cron"
        #Process "123451234512345"
      </Plugin>
  - backup: minion

collectd_tcpconns:
  file.managed:
  - name: /etc/collectd/collectd.conf.d/tcpconns.conf
  - contents: |
      <Plugin tcpconns>
             ListeningPorts false
             LocalPort "25"
             #RemotePort "5432"
      </Plugin>
  - backup: minion

default_elasticsearch:
  file.managed:
  - name: /etc/default/elasticsearch
  - contents: |
      # Start Elasticsearch automatically
      START_DAEMON=true
      
      # Run Elasticsearch as this user ID and group ID
      #ES_USER=elasticsearch
      #ES_GROUP=elasticsearch
      
      # Heap Size (defaults to 256m min, 1g max)
      #ES_HEAP_SIZE=2g
      ES_HEAP_SIZE=512m
      
      # Heap new generation
      #ES_HEAP_NEWSIZE=
      
      # max direct memory
      #ES_DIRECT_SIZE=
      
      # Maximum number of open files, defaults to 65535.
      #MAX_OPEN_FILES=65535
      
      # Maximum locked memory size. Set to "unlimited" if you use the
      # bootstrap.mlockall option in elasticsearch.yml. You must also set
      # ES_HEAP_SIZE.
      #MAX_LOCKED_MEMORY=unlimited
      
      # Maximum number of VMA (Virtual Memory Areas) a process can own
      #MAX_MAP_COUNT=262144
      
      # Elasticsearch log directory
      LOG_DIR=/var/log/elasticsearch
      
      # Elasticsearch data directory
      DATA_DIR=/var/lib/elasticsearch
      
      # Elasticsearch work directory
      WORK_DIR=/tmp/elasticsearch
      
      # Elasticsearch configuration directory
      CONF_DIR=/etc/elasticsearch
      
      # Elasticsearch configuration file (elasticsearch.yml)
      CONF_FILE=/etc/elasticsearch/elasticsearch.yml
      
      # Additional Java OPTS
      #ES_JAVA_OPTS=
      
      # Configure restart on package upgrade (true, every other setting will lead to not restarting)
      #RESTART_ON_UPGRADE=true
      
      JAVA_HOME=/usr/lib/jvm/java-7-openjdk-i386

    - backup: minion

collectd_syntax_conf:
  cmd.run:
  - name: /usr/sbin/collectd -t -C /etc/collectd/collectd.conf
  - order: last

# Following example for sudoers.d/sudoCmnd_Alias
# Cmnd_Alias      CAPACITY = /bin/df, /usr/bin/du, /usr/bin/du -sh *
# Cmnd_Alias      GRAPHITEWEB = /bin/more /etc/graphite/local_settings.py
# Cmnd_Alias      CARBON = /bin/tailf /var/log/carbon/creates.log, /usr/bin/telnet 127.0.0.1 2003

# m h  dom mon dow   command to run as root
# */3 * * * * /opt/local/venv_gexplorer/bin/update_metrics.py /home/gwright/gexplorer.cfg &> /dev/null

