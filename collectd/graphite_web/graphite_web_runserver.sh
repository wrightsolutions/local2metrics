#!/bin/sh
#chgrp _graphite /var/lib/graphite/
#chmod g+wx /var/lib/graphite/
cd /usr/share/doc/graphite-web
graphite-manage syncdb
chown _graphite:_graphite /var/lib/graphite/graphite.db
su -s /bin/bash -c '/usr/bin/django-admin runserver --settings graphite.settings 0.0.0.0:8888' _graphite
