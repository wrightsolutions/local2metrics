{% set graphite_sharedoc = salt['pillar.get']('graphite_sharedoc_filepath','/usr/share/doc/graphite-web') %}
{% set graphite_www = salt['pillar.get']('graphite_www_filepath','/tmp/graphite-web') %}
{% set graphite_group = salt['pillar.get']('graphite_wwwgroup','_graphite') %}
#{% set adminuser = salt['pillar.get']('graphite_adminuser','admin') %}
#{% set adminpassword = salt['pillar.get']('graphite_adminpassword','graphite2014') %}
{% set appname = 'cmsapp' %}

collectd5packages:
  pkg.installed:
    - pkgs:
      - collectd
      - librrd4
      - librrds-perl
    - install_recommends: false

collectd_related_packages_incl_apache2:
  pkg.installed:
    - pkgs:
      - apache2
      # - libapache2-mod-wsgi
      - libjson-perl
      - git
    # - install_recommends: false


graphite_carbon_packages:
  pkg.installed:
    - pkgs:
      - graphite-carbon
    - install_recommends: false


graphite_web_packages:
  pkg.installed:
    - pkgs:
      - rrdcached
      - graphite-web
      - libjs-jquery
      - python-rrdtool
      - python-sqlite
      - python-simplejson
      - python-tz
    - install_recommends: false


graphite_wsgi_module:
  pkg.installed:
    - pkgs:
      - libapache2-mod-wsgi
  service:
    - name: apache2
    - running
    - enable: True


#graphite_wsgi_module_enable:
  #apache_module.enable:
  #- name: wsgi


graphite_www_directory:
  file.directory:
    - name: {{ graphite_www }}/
    - user: root
    - group: {{ graphite_group }}
    - mode: 2750
    - clean: True
    - exclude_pat: '*.py'

graphite_web_runserver_sh:
  file.managed:
    - name: /tmp/graphite_web_runserver.sh
    # - name: {{ graphite_www }}/graphite_web_runserver.sh
    - contents: |
        #!/bin/sh
        cd /usr/share/doc/graphite-web
        graphite-manage syncdb
        chown _graphite:_graphite /var/lib/graphite/graphite.db
        su -s /bin/bash -c '/usr/bin/django-admin runserver --settings graphite.settings 0.0.0.0:8080' _graphite
    - order: last


collectd_core_auto_migrate4:
  debconf.set:
    - name: collectd-core
    - data:
        'collectd/auto-migrate-3-4': {'type': 'boolean', 'value': False}

collectd_core_auto_migrate5:
  debconf.set:
    - name: collectd-core
    - data:
        'collectd/auto-migrate-4-5': {'type': 'boolean', 'value': False}

