#!/bin/sh
fab --port=2222 -u root -f collectd5debian.py -H 127.0.0.1 collectd_install

#[127.0.0.1] Executing task 'collectd_install'
#[127.0.0.1] run: DEBIAN_FRONTEND=noninteractive /usr/bin/apt-get --no-install-recommends  --assume-yes  install librrd4 librrds-perl
#[127.0.0.1] run: DEBIAN_FRONTEND=noninteractive /usr/bin/apt-get --no-install-recommends  --assume-yes  install collectd

tar -C /etc/ -xf /root/collectd54parented.tar

fab --port=2222 -u root -f collectd5debian.py -H 127.0.0.1 collectd_web_py_apache_advanced > /tmp/fab.out

#[127.0.0.1] run: /usr/bin/apt-get --assume-yes  install libapache2-mod-wsgi
#[127.0.0.1] run: /usr/bin/apt-get --assume-yes  install apache2
#[127.0.0.1] run: /usr/bin/apt-get --assume-yes  install git
#[127.0.0.1] run: /usr/bin/apt-get --assume-yes  install libjson-perl
#[127.0.0.1] run: DEBIAN_FRONTEND=noninteractive /usr/bin/apt-get --no-install-recommends  --assume-yes  install graphite-carbon
#[127.0.0.1] run: /usr/bin/apt-get --no-install-recommends -y  install rrdcached graphite-web libjs-jquery python-rrdtool python-sqlite python-simplejson python-tz

