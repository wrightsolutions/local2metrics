#!/usr/bin/env python
""" When supplied with a space separated list of processes,
will generate processes.conf file for collectd.
"""

from sys import argv,exit

CHRFS=chr(47)
OPENLT=chr(60)
CLOSEGT=chr(62)
SPCPROCESS=chr(32)*2
CHRNL=chr(10)


def processes_build(process_list):
        line_open = None
        if process_list is None:
                return ''
        elif len(process_list) < 1:
                return ''
        else:
                line_open = "%sPlugin processes%s" % (OPENLT,CLOSEGT)
                line_close = "%s%sPlugin%s" % (OPENLT,CHRFS,CLOSEGT)

        lines = []
        for proc in process_list:
                """ 123451234512345
                if len(proc) > 15:
                        proc = proc[:15]
                """
                lines.append('%sProcess "%s"' % (SPCPROCESS,proc[:15]))
        procstring = line_open + CHRNL + CHRNL.join(lines) + CHRNL + line_close
	return procstring


if __name__ == "__main__":
	if len(argv) == 1:
		print "Please supply arguments!"
		exit(130)
	try:
		process_count1 = len(argv)
                process_count = process_count1-1
	except:
		exit(131)

	procstr = processes_build(argv[1:process_count1])
        if procstr is None:
                exit(141)
        elif len(procstr) > 9:
                print procstr
                print "# Above generated from %d processes" % process_count
        else:
                exit(142)
        
