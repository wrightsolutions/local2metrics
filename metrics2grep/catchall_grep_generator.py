#!/usr/bin/env python
# -*- coding: utf-8 -*-

import httplib
import re
from sys import argv,exit
from urllib2 import Request, urlopen

PORT_ES=9200
SIZE_ES=9999
DQ=chr(34)
COMMA=chr(46)
DQ_COMMA = "{0}{1}".format(DQ,COMMA)
DQ_COMMA4 = "{0}{1} {0}".format(DQ,COMMA)
DOTTY=chr(46)
EQUALS=chr(61)
RE_PREFIXED_N1 = re.compile("{0}, {0}n1=".format(DQ))
RE_DQ_COMMA = re.compile("{0}{1}".format(DQ,COMMA))
RE_DQ_COMMA4 = re.compile("{0}{1} {0}".format(DQ,COMMA))
RE_EQUALS = re.compile("{0}".format(EQUALS))
RE_NDIGIT = re.compile("n[0-9]")
#RE_CLOSE_FINAL = re.compile(".*{0}\\]\Z".format(DQ))
RE_CLOSE_FINAL = re.compile(".*{0}\\]".format(DQ))


def dottable(prefix,string_candidate):
	item_stripped = string_candidate.rstrip()
	#print "dottable with prefix is:",prefix,string_candidate
	if len(item_stripped) < 1:
		return None
	if not prefix.startswith('n'):
		return None
	if RE_CLOSE_FINAL.match(item_stripped.strip()):
		#print "CLOSE_FINAL"
		return None
	#print item_stripped
	return item_stripped


def separate_on_equals(line_unparsed):
	#print line_unparsed
	#print '###'		
	sep_array = RE_DQ_COMMA4.split(line_unparsed)
	#print(len(sep_array))
	stripped = []
	for item in sep_array:
		if len(item.strip()) < 1:
			continue
		stripped.append(item.strip())

	#print [s.strip('"') for s in sep_array]
	slist = []
	item_stored = ''
	for num,item in enumerate(stripped):
		matches2 = RE_EQUALS.split(item)
		if len(matches2) > 2:
			before = matches2[0]
			prefixed = matches2[1]
			after = matches2[2:]
			if prefixed.startswith('n'):
				#print "n", item
				pass
			if after.startswith('n'):
				#print "nn", item
				pass

			string_for_dotty = dottable(item_stored,before)
			item_stored = before
			if string_for_dotty is None or len(string_for_dotty) < 1:
				continue
			slist.append(string_for_dotty)

		elif RE_EQUALS.search(item):
			appended = 0
			before = matches2[0]
			prefixed = matches2[1]
			#after = matches2[2:]

			if before.startswith('n2'):
				slist.append(prefixed)
				appended += 1
			elif before.startswith('n'):
				#print "nnn", item
				pass
			elif prefixed.startswith('n'):
				#print "nnnn", item
				pass

			if appended < 1:
				#print item," split into short array: ",before," and ",prefixed
				if RE_NDIGIT.match(before):
					#print "{0},{1} calling string_for_dotty".format(before,prefixed)
					string_for_dotty = dottable(before,prefixed)
				else:
					string_for_dotty = dottable(item_stored,prefixed)
				item_stored = before
				if string_for_dotty is None or len(string_for_dotty) < 1:
					continue
				slist.append(string_for_dotty)

				#item_stored = item.split(EQUALS)[0]
				#slist.append(prefixed)
				item_stored = before
			else:
				pass
		elif num < 1:
			item_stripped = item.strip()
			#print "first item so setting item_stored=",item_stripped
			slist.append(item_stripped)
		else:
			#print "not matches but setting item_stored=",item
			item_stored = item
			#print item_stored
			item_stripped = item.rstrip()
			if len(item_stripped) < 1:
				continue
	return slist


def gweb_generate(line_unparsed):
	line = None
	matches0 = RE_PREFIXED_N1.split(line_unparsed)
	if matches0:
		before = matches0[0]
		prefixed = matches0[1]
		#after = matches0[2:]
		line_array = separate_on_equals(prefixed)
		line = DOTTY.join(line_array)
	return line


def reponse_in_full_counted(resp):
	lines_matching = []
	for num, line in enumerate(resp, start=1):
		lines_matching.append(line)
	return lines_matching


def reponse_to_list(target,indexname='graphite_metrics2',filter='"plugin=catchall"'):

	localhost_url = """http://localhost:{0}/graphite_metrics2/_search?pretty=1""".format(PORT_ES)
	if SIZE_ES > 10:
		localhost_url = """{0}&size={1}""".format(localhost_url,SIZE_ES)

	request = Request(localhost_url)
	try:
		response = urlopen(request)
	except IOError:
        	return []
	except BadStatusLine:
        	return []

	lines_matching = []

	if target is None:
		linecount = 0
		for line in response:
			lines_matching.append(line)
			linecount += 1
		return linecount

	if len(target) < 2:
		return reponse_in_full_counted(response)

	num = 0
	for num, line in enumerate(response, start=1):
		for line in response:
			if filter not in line:
				continue
			if target not in line:
				continue
			lines_matching.append(line)
	
	return lines_matching


if __name__ == "__main__":


	exit_rc = 0
	#if len(argv) < 2:
	#	exit(130)

	target =''
	if len(argv) > 1:
		target = argv[1]

	list_of_lines = reponse_to_list(target,indexname='graphite_metrics2')
	if len(list_of_lines) < 1:
		exit(exit_rc)

	memory_free = 0
	if 'mem' in target:
		for line in list_of_lines:
			#print line
			if memory_free < 1 and '=free' in line:
				memory_free = 1

	gweb_tailsize = "height=800&width=600"
	if memory_free > 0:
		gweb_line = "gweb:8888/render?target={0}&{1}".format("*.memory.memory.free",gweb_tailsize)
		print gweb_line
		
	for line in list_of_lines:
		search_result = RE_PREFIXED_N1.search(line)
		if not search_result:
			continue
		match = search_result.group(0)
		gweb_line = "gweb:8888/render?target={0}&{1}".format(gweb_generate(line),gweb_tailsize)
		if gweb_line:
			print gweb_line

