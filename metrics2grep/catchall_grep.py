#!/usr/bin/env python

import httplib
from sys import argv,exit
from urllib2 import Request, urlopen

SIZE_ES=9999
PORT_ES=9200

def reponse_in_full_counted(resp):
	num = 0
	for num, line in enumerate(resp, start=1):
		print line,
	return num


def parse_reponse(target,indexname='graphite_metrics2',filter='"plugin=catchall"'):

	localhost_url = """http://localhost:{0}/graphite_metrics2/_search?pretty=1""".format(PORT_ES)
	if SIZE_ES > 10:
		localhost_url = """{0}&size={1}""".format(localhost_url,SIZE_ES)

	request = Request(localhost_url)
	try:
		response = urlopen(request)
	except IOError:
        	response = []
	except BadStatusLine:
        	response = []

	if target is None:
		linecount = 0
		for line in response:
			print line,
			linecount += 1
		return linecount

	if len(target) < 2:
		return reponse_in_full_counted(response)

	num = 0
	for num, line in enumerate(response, start=1):
		for line in response:
			if filter not in line:
				continue
			if target not in line:
				continue
			print line,
	
	return num


if __name__ == "__main__":


	exit_rc = 0
	#if len(argv) < 2:
	#	exit(130)

	target =''
	if len(argv) > 1:
		target = argv[1]

	parse_reponse(target,indexname='graphite_metrics2')
